import React, { Component } from 'react';
import { Text, TextInput, View ,Button,StyleSheet} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

class log extends Component {
  constructor(props) {
    super(props);
    this.state = {username: ''};
    this.state = {password: ''};
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={{height: 40}}
          placeholder="username"
          onChangeText={(username) => this.setState({username})}
          value={this.state.username}
        />
      <TextInput
          style={{height: 40}}
          placeholder="password"
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
       <Button title="login" style={{margin: 50}} onPress={() => this.props.navigation.navigate('login',{ username: this.state.username})}/>
      </View>
    );
  }
}


class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Hey {this.props.navigation.state.params.username}</Text>
      </View>
    );
  }
}
const RootStack = createStackNavigator(
  {
    Home: log,
    login: DetailsScreen,
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'

  },
});