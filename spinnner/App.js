import React,{Component} from 'react';
import { ActivityIndicator,StyleSheet,Button, View } from 'react-native';

export default class spinner extends Component
{
  state={
    running:true
  }
  render(){
    return(
    <View style={[styles.container]}>
    <ActivityIndicator size='large' color="#0000ff" animating={this.state.running}/>
    <Button title="click"  color="#f194ff" onPress={()=>{this.state.running ? this.setState({running:false}):this.setState({running:true})}}/>
    </View>

    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  }
});
